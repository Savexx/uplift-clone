const Navbar = () => {
  return (
    <nav className='nav'>
      <img
        src='https://www.upliftcodecamp.com/_next/image?url=%2Fimages%2Fuplift-nav.png&w=3840&q=75'
        alt=''
      />
    </nav>
  );
};

export default Navbar;
