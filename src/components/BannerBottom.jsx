import Button from './Button';

const BannerBottom = () => {
  return (
    <footer className='BannerBottom'>
      <p>
        Ready to reach for your dreams? <span>Start today. 🚀</span>
      </p>
      <Button />
    </footer>
  );
};

export default BannerBottom;
