import Button from './Button';

const Hero = () => {
  return (
    <div className='hero'>
      <h1>
        Launch your carrer
        <span>in software development</span>
      </h1>

      <h2>
        Affordable, high-quality web development code camp. Learn to code
        full-stack with one-on-one coaching and the support of career mentors.
      </h2>

      <Button />
    </div>
  );
};

export default Hero;
