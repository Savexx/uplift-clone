import Navbar from './components/Navbar';
import Hero from './components/Hero';
import BannerBottom from './components/BannerBottom';

function App() {
  return (
    <div className='App'>
      <Navbar />
      <Hero />
      <BannerBottom />
    </div>
  );
}

export default App;
